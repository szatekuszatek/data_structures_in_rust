const MAX_SIZE: usize = 1024;

struct Stack {
    inner: [u8;MAX_SIZE],
    top: usize,
}

impl Stack {
    fn new() -> Self {
        Stack { inner: [0;MAX_SIZE], top: 0 }
    }

    fn pop(&mut self) -> Option<u8> {
        let popped = self.inner[self.top];
        if self.top > 0 {
            self.top -= 1;        
            return Some(popped.to_owned());
        }
        return None;
    }

    fn push(&mut self, pushed: u8) -> Result<(), ()> {
        if self.top < MAX_SIZE - 1 {
            self.top += 1;
            self.inner[self.top] = pushed;
            return Ok(())
        }
        return Err(());
    }
} 

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn puts_on_top() {
        let mut stck = Stack::new();
        stck.push(42);
        assert!(stck.top == 1);
        assert!(stck.inner[stck.top] == 42);
    }

    #[test]
    fn does_not_overflow() {
        let mut stck = Stack::new();
        for i in 0..(MAX_SIZE + 1) {
            stck.push(42);
        }
        assert!(stck.top == MAX_SIZE - 1);
    }
}

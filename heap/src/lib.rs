use std::cmp;

const MAX_SIZE: usize = 1024;

#[derive(Debug, PartialEq)]
enum HeapOperationError {
    InsertIntoFull,
    RemoveFromEmpty,
}

#[derive(Debug)]
struct BinaryHeap {
    inner: [u8; MAX_SIZE],
    size: usize,
}

impl BinaryHeap {
    fn new() -> Self {
        Self {
            inner: [0; MAX_SIZE],
            size: 0,
        }
    }

    fn with_n_items(n: usize, item: u8) -> Self {
        let mut b_heap = Self {
            inner: [0; MAX_SIZE],
            size: 0,
        };
        if n <= 0 {return b_heap}

        for i in 0..n {
            b_heap.insert(item);
        }
        b_heap
    }

    fn is_empty(&self) -> bool {
        self.size == 0
    }

    fn insert(&mut self, item: u8) -> Result<(), HeapOperationError> {
        if self.size >= MAX_SIZE {
            return Err(HeapOperationError::InsertIntoFull);
        }
        // insert at the end of the heap
        self.inner[self.size] = item;

        // swim operation
        let mut ptr = self.size;
        while ptr > 0 {
            let parent_ptr = (ptr - 1) / 2;
            if self.inner[ptr] > self.inner[parent_ptr] {
                self.inner.swap(ptr, parent_ptr);
            } else {
                break;
            }
            ptr -= 1;
        }

        self.size += 1;
        return Ok(());
    }

    fn extract_max(&mut self) -> Option<u8> {
        if self.size == 0 {
            return None;
        }

        let temp = self.inner[0];
        self.inner[0] = cmp::max(self.inner[1], self.inner[2]);
        Some(temp)
    }

    fn detele_max(&mut self) -> Result<(), HeapOperationError> {
        if self.size == 0 {
            return Err(HeapOperationError::RemoveFromEmpty);
        }

        _ = self.extract_max();
        Ok(())
    }
    
    fn merge(&self, heap: &Self) -> Self {
        let mut new_heap = Self::new();
        // zipping the heaps to minimize swaps
        for el in self.inner.iter().zip(heap.inner.iter()) {
            new_heap.insert(el.0.clone());
            new_heap.insert(el.1.clone());
        }
        new_heap 
    }
}

macro_rules! binary_heap {
    () => (
        $crate::BinaryHeap::new()
    );
    ($($x:expr),*) => {{
        let mut heap = $crate::BinaryHeap::new();
        $(
            heap.insert($x);
        )*
        heap
    }};
    ($($x:expr,)*) => (binary_heap![$($x),*]);
    ($item:expr; $n:expr) => ($crate::BinaryHeap::with_n_items($n, $item));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn performs_swim() {
        let mut heap = binary_heap![1, 2, 3];

        assert_eq!(heap.inner[0], 3);
        assert_eq!(heap.inner[1], 1);
        assert_eq!(heap.inner[2], 2);
    }

    #[test]
    fn errors_instead_overflow() {
        let mut heap = binary_heap![0; MAX_SIZE];

        assert_eq!(heap.insert(0), Err(HeapOperationError::InsertIntoFull));
    }

    #[test]
    fn extracts_max() {
        let mut heap = binary_heap![1, 2, 3];

        assert_eq!(heap.extract_max(), Some(3));
        assert_eq!((heap.inner[0], heap.inner[1]), (2, 1));
    }

    #[test]
    fn merges_heaps() {
        let mut heap_1 = binary_heap![1, 2, 3];
        let mut heap_2 = binary_heap![1, 2, 3];
        let merged = heap_1.merge(&heap_2);

        assert!(heap_1.size == 3);
        assert!(heap_2.size == 3);
        assert_eq!(merged.inner, binary_heap![3,3,2,1,2,1].inner);
    }
}

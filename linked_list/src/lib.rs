#[derive(Clone)]
struct Node {
    key: String,
    value: u8,
    next: Option<Box<Node>>,
}

struct SingleLinkedList {
    head: Option<Box<Node>>,
    size: usize,
}

impl SingleLinkedList {
    fn new() -> Self {
        Self {
            head: None,
            size: 0,
        }
    }
    
    fn append(&mut self, new_node: Node) {
        match &mut self.head {
            Some(n) => {
                let mut curr_node = n;
                loop {
                    if curr_node.next.is_some() {
                        curr_node = curr_node.next.as_mut().unwrap();
                        continue;
                    }
                    break;
                }
                curr_node.next = Some(Box::new(new_node));
            },
            None => self.head = Some(Box::new(new_node)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn append_list() {
        let n = Node { key: "Life".to_string(), value: 42, next: None };
        let mut list = SingleLinkedList::new();
        list.append(n.clone());
        assert_eq!(list.head.unwrap().value, n.value);
    }
}
